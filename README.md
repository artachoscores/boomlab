![Sibelius-logo](https://bitbucket.org/artachoscores/brainchild/raw/HEAD/sibelius_logo_trans.png)

# BoomLab

Series of Ètudes for the BoomLab (as part of the _Atlas of Smooth Spaces_). These artistic experiments ultimaltely refer to the performative format put forth in the [New Artform Manifesto](https://docs.google.com/document/d/e/2PACX-1vSgoLASfOjhBk0aHl2RdRH4YJ_fjLPUsAErZawkbQY69Fp5jp1hvMC6Ef_keps-pSyfc4-l8ksIhR-Y/pub).

## Ètudes

One of the Ètudes could use **stereotypical** voicings common in a movie set to achieve a seamless transition from the *non-piece-yet* starting point, towards a highly structured texture where this source material is developed. E.g.: *action!*, *ready?*, *lights set*, *audio*, *on your marks*, *rolling!*... etc. (the last voicing heard could be *action!*, as in *Schönberg in Hollywood*).

The text for one of the Ètudes could be from Deleuze (smoothness/image-temps image-movement)...

Another Ètude could focus on the Boom movements, and the acoustic artifacts.

Another Ètude in open air, with a slightly larger amount of people.

Synergy with **ChorLab** (Hiemetsberger): piece for singers (combined or not with additional performers) 

## Acoustic artefacts (to experiment with)

* **'Transit'**: Kind of interference that happens when a boom moves from an active source to another

* Sound **'Compression/Expansion'** when boomer runs towards/away from the source

* **'Zooming in/out'**. Playful roaming between *foreground* and *background* sound.

For the projection of the sound, there's some strategies:

+ 1 boom = 1 speaker

+ 1 boom = 1 coordinate for panning...

## Ideas

Make **boom parts** for the boom operators, that simply indicate them (auditory beats `01 11 12`) whan to swap and move to another actor.

Convention: **boxed text** is to be repeated, whereas *not-boxed* text is only to be performed once, however improvisatory.

Quasi-free text to be read, with specific beats **synchronised** accross all performers *alla* Share Care Dare.

Perform a **random held note** (pitch is free for the performers to decide). This could be mocked up by having a "random" feature, so that in Live it can choose randomly from a bunch of different clips ([Tesser_clips](https://bitbucket.org/AdrianArtacho/tesser_clips/src) derivative).

BACKWARDS TIME: make music - render action score - reverse the actions - shoot the video (and record audio) - reverse the time of the footage and make it fit to the original composition

BACK-AND-FORTH TIME: same as before, alternating reverse with regular time

## Score

Image size should be optimised for smartphone devices (**16:9**). Here are the _slideshows_ that hosts the image files in the cloud:

[BoomLab-E1](https://docs.google.com/presentation/d/1aiLjcQhbzw2sfP3D4W179BQkUR5ecO1CpvntvCPludI/edit#slide=id.p)

You may download the whole slideshow as *.pdf* and use [imagemagick](https://imagemagick.org/script/download.php#macosx) to convert all slides to image files. Alternatively, there are online services like [pdf2png.com](https://pdf2png.com/)
which do just the same. These programs number the slides starting t 1. An additional google drawing is needed for the cover page (page 0).

[BoomLab-E1-00](https://docs.google.com/drawings/d/1XiGErN2LFPhSYqg2Pw3rvssEcgEuVYwjPFCvpoywmV8/edit)

## Technical Implementation

### Pseudocode

The details of the implementatioon at the *morphological* level (how cues are generated on the performer side) and the *syntactic* level (how cues relate to one another within the scope of the piece) are codified in a human readable, remote spreadsheet in google drive. Here are the links to said files:

[BoomEtude: spreadsheet](https://docs.google.com/spreadsheets/d/1ZjVRB3ZWQ4dwoQwapLxIsmNX9o766IJoAnWpXNTowPU/edit#gid=377593973)

____

# To-Do

- [Route Map](https://drive.google.com/file/d/1_BTctfT--SsL3A7N3K-CeugokXBRYNkR/view?usp=sharing)
- Test the system using amazon (no router)
- Introduce the audio feature!!
- Write down [pitch text](https://docs.google.com/document/d/13VEq3MUuj9PZ2pCSqtvcygXvU95Y05LBC8y2Q3MYM6o/edit#heading=h.t3mp7m18lm5i)
